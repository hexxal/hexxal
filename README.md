# Hexxal

## Getting Started
*Come up with some fluff for this later.*

1. Clone the repo
2. Run `npm i` to install dependencies
3. Launch the app with `npm start`

## Features List
A more complete list of features and milestones will eventually land its way in [the repo's issue list](https://gitlab.com/mwarnerdotme/hexxal/issues). But if you insist...

### Pre-v1.0 and v1.0
* Drag audio files into the application to process and store in the library
* “Safe Mode” for copying files to library, “Lean Mode” for moving files to library
* Manually edit track metadata individually or in batches
* Double click will open the file in the user’s music player of choice
* Drag action will open the file in any program the row is dragged into (like Splice)
### v1.1
* Convert audio from one format to another at the click of a button
### v1.2
* Add some basic audio manipulation effects ([Audacity](https://github.com/audacity/audacity) for inspiration)
### v1.3
* Create playlists/setlists from files in the library
* Setlist + library export (to .zip or external media)
### v1.4
* User defined “recipes” for how their library should be organized
### v1.5
* Build ReplayGain tagging functionality into the app
### v1.6
* Manually attach images (album art) to individual tracks
* Discogs integration for metadata suggestion and album art fills
